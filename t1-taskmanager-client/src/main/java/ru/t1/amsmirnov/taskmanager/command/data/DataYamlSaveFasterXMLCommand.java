package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataYamlSaveFasterXMLRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataYamlSaveFasterXMLResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class DataYamlSaveFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-yaml";

    @NotNull
    public static final String DESCRIPTION = "Save data to YAML file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE YAML]");
        @NotNull final DataYamlSaveFasterXMLRequest request = new DataYamlSaveFasterXMLRequest(getToken());
        @NotNull final DataYamlSaveFasterXMLResponse response = getDomainEndpoint().saveDataYAMLFasterXML(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
