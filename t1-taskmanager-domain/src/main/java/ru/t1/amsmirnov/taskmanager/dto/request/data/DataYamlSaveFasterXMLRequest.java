package ru.t1.amsmirnov.taskmanager.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class DataYamlSaveFasterXMLRequest extends AbstractUserRequest {

    public DataYamlSaveFasterXMLRequest() {
    }

    public DataYamlSaveFasterXMLRequest(@Nullable String token) {
        super(token);
    }
}
