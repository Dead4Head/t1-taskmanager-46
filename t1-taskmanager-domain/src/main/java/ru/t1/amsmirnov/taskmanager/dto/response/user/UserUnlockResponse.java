package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class UserUnlockResponse extends AbstractResultResponse {

    public UserUnlockResponse() {
    }

    public UserUnlockResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}