package ru.t1.amsmirnov.taskmanager.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public interface IProjectTaskDtoService {

    void bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws AbstractException;

    void removeProjectById(
            @Nullable String userId,
            @Nullable String projectId
    ) throws AbstractException;

    void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws AbstractException;

}
