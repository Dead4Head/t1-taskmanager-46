package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IProjectDtoRepository;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDTO> implements IProjectDtoRepository {

    public ProjectDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        final String jql = "SELECT p FROM ProjectDTO p ORDER BY p.created";
        return entityManager.createQuery(jql, ProjectDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSorted(@Nullable final String sort) {
        if (sort == null || sort.isEmpty()) return findAll();
        final String jql = "SELECT p FROM ProjectDTO p ORDER BY :sort";
        return entityManager.createQuery(jql, ProjectDTO.class)
                .setParameter("sort", sort)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        final String jql = "SELECT p FROM ProjectDTO p WHERE p.user_id = :userId ORDER BY p.created";
        return entityManager.createQuery(jql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSorted(
            @NotNull final String userId,
            @Nullable final String sort
    ) {
        if (sort == null || sort.isEmpty()) return findAll(userId);
        final String jql = "SELECT p FROM ProjectDTO p WHERE p.user_id = :userId ORDER BY :sort";
        return entityManager.createQuery(jql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("sort", sort)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String id) {
        final String jql = "SELECT p FROM ProjectDTO p WHERE p.id = :id";
        return entityManager.createQuery(jql, ProjectDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        final String jql = "SELECT p FROM ProjectDTO p WHERE p.user_id = :userId AND p.id = :id";
        return entityManager.createQuery(jql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);

    }

    @Override
    public void removeAll() {
        @NotNull final String jql = "DELETE FROM ProjectDTO";
        entityManager.createQuery(jql).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final String jql = "DELETE FROM ProjectDTO p WHERE p.user_id = :userId";
        entityManager.createQuery(jql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
