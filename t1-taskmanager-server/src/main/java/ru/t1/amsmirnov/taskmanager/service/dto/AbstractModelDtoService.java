package ru.t1.amsmirnov.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IAbstractDtoRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IAbstractDtoService;
import ru.t1.amsmirnov.taskmanager.comparator.NameComparator;
import ru.t1.amsmirnov.taskmanager.comparator.StatusComparator;
import ru.t1.amsmirnov.taskmanager.dto.model.AbstractModelDTO;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractModelDtoService<M extends AbstractModelDTO, R extends IAbstractDtoRepository<M>>
        implements IAbstractDtoService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractModelDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract R getRepository(@NotNull final EntityManager entityManager);

    @NotNull
    @Override
    public M add(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractDtoRepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.add(model);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    public Collection<M> addAll(@Nullable final Collection<M> models) throws AbstractException {
        if (models == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractDtoRepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.addAll(models);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@Nullable final Collection<M> models) throws AbstractException {
        if (models == null) throw new ModelNotFoundException();
        removeAll();
        addAll(models);
        return models;
    }

    @NotNull
    @Override
    public  List<M> findAll() throws AbstractException {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractDtoRepository<M> repository = getRepository(entityManager);
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public  List<M> findAll(@Nullable final Comparator<M> comparator) throws AbstractException {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractDtoRepository<M> repository = getRepository(entityManager);
        try {
            return repository.findAllSorted(getComparator(comparator));
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractDtoRepository<M> repository = getRepository(entityManager);
        try {
            final M model = repository.findOneById(id);
            if (model == null) throw new ModelNotFoundException();
            return model;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public M update(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractDtoRepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(model);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    public M removeOne(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractDtoRepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(model);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    public M removeOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractDtoRepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            final M model = repository.findOneById(id);
            if(model == null) throw new ModelNotFoundException();
            repository.remove(model);
            transaction.commit();
            return model;
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractDtoRepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.removeAll();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() throws AbstractException {
        return findAll().size();
    }

    @Override
    public boolean existById(@NotNull final String id) throws AbstractException {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IAbstractDtoRepository<M> repository = getRepository(entityManager);
        try {
            repository.findOneById(id);
            return true;
        } catch (final Exception e) {
            return false;
        } finally {
            entityManager.close();
        }
    }

    protected String getComparator(Comparator<?> comparator) {
        if (comparator instanceof StatusComparator) return "status";
        if (comparator instanceof NameComparator) return "name";
        return "created";
    }
    
}
