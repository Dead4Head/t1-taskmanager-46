package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IAbstractUserOwnedDtoRepository;
import ru.t1.amsmirnov.taskmanager.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO>
        extends AbstractDtoRepository<M>
        implements IAbstractUserOwnedDtoRepository<M> {

    public AbstractUserOwnedDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}
