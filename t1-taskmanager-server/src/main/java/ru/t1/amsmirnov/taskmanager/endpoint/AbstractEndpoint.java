package ru.t1.amsmirnov.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.service.IAuthService;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.user.AccessDeniedException;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;

public abstract class AbstractEndpoint {

    @NotNull
    protected final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request, @Nullable final Role role) throws AbstractException {
        if (request == null || role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @Nullable final SessionDTO session = authService.validateToken(token);
        boolean check = session.getRole() != null && session.getRole().equals(role);
        if (!check) throw new AccessDeniedException();
        return session;
    }

    protected SessionDTO check(@Nullable final AbstractUserRequest request) throws AbstractException {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return getServiceLocator().getAuthService().validateToken(token);
    }

}
