package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IProjectDtoRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;
import ru.t1.amsmirnov.taskmanager.enumerated.ProjectSort;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.repository.dto.ProjectDtoRepository;
import ru.t1.amsmirnov.taskmanager.service.ConnectionService;
import ru.t1.amsmirnov.taskmanager.service.PropertyService;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String NONE_ID = "---NONE---";

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private List<ProjectDTO> projects;

    @NotNull
    private List<ProjectDTO> alfaProjects;

    @NotNull
    private List<ProjectDTO> betaProjects;

    @NotNull EntityManager entityManager;

    @NotNull EntityTransaction transaction;

    @NotNull
    private IProjectDtoRepository projectRepository;

    @Before
    public void initRepository() {
        entityManager = CONNECTION_SERVICE.getEntityManager();
        transaction = entityManager.getTransaction();
        projectRepository = new ProjectDtoRepository(entityManager);
        projects = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("ProjectDTO " + (NUMBER_OF_ENTRIES - i));
            project.setDescription("Description " + i);
            if (i < 5)
                project.setUserId(USER_ALFA_ID);
            else
                project.setUserId(USER_BETA_ID);
            try {
                transaction.begin();
                projectRepository.add(project);
                projects.add(project);
                transaction.commit();
            } catch (final Exception e) {
                transaction.rollback();
                throw e;
            }
        }
        projects = projects.stream()
                .sorted(ProjectSort.BY_CREATED.getComparator())
                .collect(Collectors.toList());
        alfaProjects = projects
                .stream()
                .filter(p -> p.getUserId().equals(USER_ALFA_ID))
                .collect(Collectors.toList());
        betaProjects = projects
                .stream()
                .filter(p -> p.getUserId().equals(USER_BETA_ID))
                .collect(Collectors.toList());
    }

    @After
    public void clearRepository() {
        try {
            projectRepository.removeAll();
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        }
        projects.clear();
    }

    @Test
    public void testAdd() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Added project");
        project.setDescription("Added description");
        project.setUserId(USER_ALFA_ID);
        try {
            projectRepository.add(project);
            projects.add(project);
            @Nullable final List<ProjectDTO> actualProjectList = projectRepository.findAll();
            assertEquals(projects, actualProjectList);
        } catch (final Exception e) {
            entityManager.close();
            throw e;
        }
    }

    @Test
    public void testRemoveAll() {
        try {
            assertNotEquals(0, projectRepository.findAll().size());
            projectRepository.removeAll();
            assertEquals(0, projectRepository.findAll().size());
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveAllForUser() {
        try {
            assertNotEquals(0, projectRepository.findAll(USER_ALFA_ID).size());
            projectRepository.removeAll(USER_ALFA_ID);
            assertEquals(0, projectRepository.findAll(USER_ALFA_ID).size());
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @Test
    public void testFindAll() {
        try {
            List<ProjectDTO> actualAlfaProjects = projectRepository.findAll(USER_ALFA_ID);
            List<ProjectDTO> actualBetaProjects = projectRepository.findAll(USER_BETA_ID);
            assertNotEquals(actualAlfaProjects, actualBetaProjects);
            assertEquals(alfaProjects, actualAlfaProjects);
            assertEquals(betaProjects, actualBetaProjects);
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @Test
    @Ignore
    public void testFindAllComparator() {
        try {
            @NotNull final Comparator<ProjectDTO> comparator = ProjectSort.BY_NAME.getComparator();
            List<ProjectDTO> actualAlfaProjects = projectRepository.findAllSorted(USER_ALFA_ID, "name");
            alfaProjects = alfaProjects.stream().sorted(comparator).collect(Collectors.toList());
            assertEquals(alfaProjects, actualAlfaProjects);
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @Test
    public void testFindOneById() {
        try {
            for (final ProjectDTO project : projects) {
                assertEquals(project, projectRepository.findOneById(project.getUserId(), project.getId()));
            }
            assertNull(projectRepository.findOneById(USER_ALFA_ID, NONE_ID));
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        }
    }

}
