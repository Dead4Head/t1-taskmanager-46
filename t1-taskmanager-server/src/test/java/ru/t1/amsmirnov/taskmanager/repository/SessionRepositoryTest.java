package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.ISessionDtoRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.repository.dto.SessionDtoRepository;
import ru.t1.amsmirnov.taskmanager.service.ConnectionService;
import ru.t1.amsmirnov.taskmanager.service.PropertyService;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Category(DBCategory.class)
public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String NONE_ID = "---NONE---";

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private static final Date TODAY = new Date();

    @NotNull
    private final List<SessionDTO> sessions = new ArrayList<>();

    private ISessionDtoRepository sessionRepository;

    @NotNull EntityManager entityManager;

    @NotNull EntityTransaction transaction;

    @Before
    public void initRepository() throws Exception {
        entityManager = CONNECTION_SERVICE.getEntityManager();
        transaction = entityManager.getTransaction();
        sessionRepository = new SessionDtoRepository(entityManager);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final SessionDTO session = new SessionDTO();
            session.setCreated(TODAY);
            if (i <= 5)
                session.setUserId(USER_ALFA_ID);
            else
                session.setUserId(USER_BETA_ID);
            try {
                sessions.add(session);
                sessionRepository.add(session);
            } catch (final Exception e) {
                transaction.rollback();
                throw e;
            }
        }
    }

    @After
    public void clearRepository() throws Exception {
        try {
            sessionRepository.removeAll();
            sessions.clear();
        } catch (final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
